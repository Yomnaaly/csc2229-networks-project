var connections = [];
function getConnections(){
  return connections;
}

 function activity(sliderValue){
    connections = makeConnections();
    drawConnections(connections);    
    getConnections();
    return connections;
 }


 function makeConnections(){
  var tempLinks = links;
  numConnections =  5;
  var drawLinks = [];
  var randomNum1;
  var randomNum2;

      for (var i = numConnections - 1; i >= 0; i--) {

        if(selectedFile === "k6"){
          randomNum1 = Math.floor(1+Math.random() * 53);
          randomNum2 = Math.floor(1+Math.random() * 53);
        }
        else if(selectedFile === "k4"){
          randomNum1 = Math.floor(1+Math.random() * 16);
          randomNum2 = Math.floor(1+Math.random() * 16);
        }
        else{
          randomNum1 = Math.floor(1+Math.random() * 128);
          randomNum2 = Math.floor(1+Math.random() * 128);
        }

        var h1 = "h"+randomNum1;
        var h2 = "h"+randomNum2;
    
        drawLinks.push(connection(h1, h2, tempLinks));
      }
      return drawLinks;
 }


 function drawConnections(drawLinks){
   d3.selectAll(".node circle").classed("congestion", function(s){
      
      congestedLink = drawLinks[0][0];
     //if((showAnomaly === 1 && (pLoss === true || cong === true)) || (showAll === true && showAnomaly === 1)){
      return s === drawLinks[0][0].source.node || s === drawLinks[0][0].target.node;

   // }
   // else{
      
      //svg.selectAll(".nodes").remove();
    //}
  });

  d3.selectAll(".node circle").classed("packetLoss", function(s){
    //if((showAnomaly === 1 && pLoss === true) || (showAll === true && showAnomaly === 1)){
      pLossLink = drawLinks[pLossNodeType1][pLossNode1];
      return s === drawLinks[pLossNodeType1][pLossNode1].source.node || s === drawLinks[pLossNodeType1][pLossNode1].target.node;
    //}
    //else if(showAnomaly !== 1 && ({
      //svg.selectAll(".nodes").remove();
    //}
  });

  d3.selectAll(".link").classed("active", function(s){

 //if((showAnomaly === 1 && (pLoss === true || cong === true)) || showAll === true){
    return s === drawLinks[0][0] ||
           s === drawLinks[0][1] || 
           s === drawLinks[0][2] || 
           s === drawLinks[0][3] || 
           s === drawLinks[0][4] || 
           s === drawLinks[0][5] ||

          s === drawLinks[1][0] || 
          s === drawLinks[1][1] || 
          s === drawLinks[1][2] || 
          s === drawLinks[1][3] ||
          s === drawLinks[1][4] || 
          s === drawLinks[1][5] ||

          s === drawLinks[2][0] || 
          s === drawLinks[2][1] || 
          s === drawLinks[2][2] || 
          s === drawLinks[2][3] ||
          s === drawLinks[2][4] || 
          s === drawLinks[2][5] ||

          s === drawLinks[3][0] || 
          s === drawLinks[3][1] || 
          s === drawLinks[3][2] || 
          s === drawLinks[3][3] ||
          s === drawLinks[3][4] || 
          s === drawLinks[3][5] ||

          s === drawLinks[4][0] || 
          s === drawLinks[4][1] || 
          s === drawLinks[4][2] || 
          s === drawLinks[4][3] ||
          s === drawLinks[4][4] || 
          s === drawLinks[4][5]; 
    // }
    // else{
    //   svg.selectAll(".links").remove();
    // }
  });
}