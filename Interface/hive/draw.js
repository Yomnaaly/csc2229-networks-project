function draw(tempNodes, tempLinks){
  
	 // Initialize the info display.
	svg.selectAll(".links").remove();
	svg.selectAll(".nodes").remove();
	svg.selectAll(".axis").remove();

  var topo = selectedFile.split("k");


  var info = d3.select("#info")
      .text(defaultInfo = "k = "+topo[1] +" fat tree network topology ");

   callSlider(slider);


  // Set the radius domain.
   radius.domain(d3.extent(nodes, function(d) {return d.index;}));

  // Draw the axes.
  svg.selectAll(".axis")
      .data(nodesByType)
      .enter().append("line")
      .attr("class", "axis")
      .attr("transform", function(d) {return "rotate(" + degrees(angle(d.key)) + ")"; })
       .attr("x1", radius(-1))
       .attr("x2", function(d) { return radius(d.count-1); });

//Draw the links.
  svg.append("g")
      .attr("class", "links")
    .selectAll(".link")
      .data(tempLinks)
    .enter().append("path")
      .attr("class", "link")
      .attr("d", link()
      .angle(function(d) { return angle(d.node.type); })
      .radius(function(d) { return radius(d.node.index/1.5); }))
      .on("mouseover", linkMouseover)
      .on("mouseout", mouseout);

  var div = d3.select("#chart").append("div") 
    .attr("class", "tooltip")       
    .style("opacity", 0);

  var infobox = d3.select("#chart").append("div") 
    .attr("id", "infobox")       
    .style("opacity", 0);

  var title = infobox.append("p")
  .attr("id", "title")
  
  var bodytext = infobox.append("p")
  .attr("id", "bodytext");

  // Draw the nodes. Note that each node can have up to two connectors,
  // representing the source (outgoing) and target (incoming) links.
  svg.append("g")
      .attr("class", "nodes")
    .selectAll(".node")
      .data(tempNodes)
    .enter().append("g")
      .attr("class", "node")
      .attr("id", function(d){return d.name})
      .style("fill", function(d) { return color(d.type); })
    //.selectAll("circle")
      .append("circle")
      .attr("transform", function(d){ return "rotate(" +  degrees(angle(d.type)) + ")";})
      .attr("cx", function(d){return radius(d.index/1.5);})
      .attr("r", 4)
      .on("mouseover", function(d) {
        div.transition()    
            .duration(100)    
            .style("opacity", 1);

        var text = d.name;
        if(d3.select(this).classed("congestion")){
          text = d.name +": "+ "\n"+"congestion detected ";
        }
        else if(d3.select(this).classed("packetLoss")){
          text = d.name +": "+ "\n"+"packet loss detected ";
        }
            div.text(text)
            .style("left", (d3.event.pageX) + "px")   
            .style("top", (d3.event.pageY - 50) + "px");
        if(!d3.select(this).classed("congestion") && !d3.select(this).classed("packetLoss")){
          d3.select(this).classed("active", true);  
        }
              
      })          
      .on("mouseout", function(d) { 
          svg.selectAll(".node circle").classed("active", false);  
          div.transition()    
              .duration(50)    
              .style("opacity", 0);
          infobox.transition()    
              .duration(50)    
              .style("opacity", 0);
      })
      .on("click", function(d){

        infobox.style("visibility","visible");
        var titleText;
        var bodyTextz;
        var whatType;
        var lastText;
        var otherIp;

         infobox.transition()    
            .duration(100)    
            .style("opacity", 1);
        if(d3.select(this).classed("congestion")){

          if(d === congestedLink.source.node){
            
            whatType = "source";
          }
          else{
            whatType = "destination";
          }

          if(whatType === "source"){
            lastText = "destination: "+congestedLink.target.node.name;
            otherIp = "destination ip: "+congestedLink.target.node.ip_address;
          }
          else{
            lastText = "source: "+congestedLink.source.node.name;
            otherIp = "source ip: "+congestedLink.source.node.ip_address;
          }
          
          titleText = d.name;
          bodyTextz = "issue: congestion" +
          "<br>"+"<br>"+"time: "+Math.abs(d3.round(val))+ " seconds ago"+
          "<br>"+"<br>"+"node type: "+ whatType +
          "<br>"+"<br>"+"ip: "+d.ip_address +
          "<br>"+"<br>"+lastText+
          "<br>"+"<br>"+otherIp;
         }
        else if(d3.select(this).classed("packetLoss")){
          if(d === pLossLink.source.node){
            
            whatType = "source";
          }
          else{
            whatType = "destination";
          }

          if(whatType === "source"){
            lastText = "destination: "+pLossLink.target.node.name;
            otherIp = "destination ip: "+pLossLink.target.node.ip_address;
          }
          else{
            lastText = "source: "+pLossLink.source.node.name;
            otherIp = "source ip: "+pLossLink.source.node.ip_address;
          }

          titleText = d.name;
          bodyTextz = "issue: packet loss" +
          "<br>"+"<br>"+"time: "+Math.abs(d3.round(val))+ " seconds ago"+
          "<br>"+"<br>"+"node type: "+ whatType +
          "<br>"+"<br>"+"ip: "+d.ip_address +
          "<br>"+"<br>"+lastText+
          "<br>"+"<br>"+otherIp;
        }
        else{
          infobox.style("visibility", "hidden");
        }

        title.text(titleText);
        bodytext.html(bodyTextz);
          infobox.style("left", "calc(30%)")   
            .style("top", "calc(20%)");
      });
      
}