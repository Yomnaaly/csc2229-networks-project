function connection(host1, host2){

	connected_links = [];
	var edge1;
	var host_edge1;
	var host_edge2;
	var edge_agg1;
	var edge_agg2;
	var agg_core1;
	var agg_core2;
	var randomCore;

	if(selectedFile === "k6"){
		var randomNum = Math.floor(Math.random() * 9);
		if(randomNum <= 6){
			randomCore = (randomNum+3);
		}
		else{
			randomCore = randomNum;
		}
	}
	else if(selectedFile === "k4"){
		var randomNum = Math.floor(1+Math.random() * 4);
		while(randomNum%2 != 0){
			randomNum = Math.floor(1+Math.random() * 4);
		}
		randomCore = randomNum;
	}
	else{
		var randomNum = Math.floor(1+Math.random() * 16);
			while(randomNum%4 != 0){
				randomNum = Math.floor(1+Math.random() * 16);
			}
			randomCore = randomNum;
	}

	links.forEach(function(link){
		if(link.target.node.name === host1){
			edge1 = link.source.node.name;

			host_edge1 = link;
		}
	});

	var edge2;
	links.forEach(function(link){
		if(link.target.node.name === host2){
			host_edge2 = link;
			edge2 = link.source.node.name;
		}
	});

	var agg1;
	links.forEach(function(link){
		if(link.target.node.name === edge1){
			edge_agg1 = link;
			
			 agg1 = link.source.node.name;
			 
		}
	});

	var agg2;
	links.forEach(function(link){
		if(link.target.node.name === edge2){
			edge_agg2 = link;
			 agg2 = link.source.node.name;
			 
		}
	});

	var stop1 = false;
	var core1;
	links.forEach(function(link){
		if(link.target.node.name === agg1){
			
			 coreNum = link.source.node.name.split("e");
			 coreNum = parseInt(coreNum[1]);

			 if(coreNum <= randomCore && stop1 === false){
			 	core1 = link.source.node.name;
			 	stop = true;
			 	agg_core1 = link;
			 }
		}
	});

	var stop2 = false;
	var core2;
	links.forEach(function(link){
		if(link.target.node.name === agg2){
			core2 = link.source.node.name;

			if(core1 === core2 && stop2 === false){
				agg_core2 = link;
				stop2 = true;
			}
		}
	});

	connected_links.push(host_edge1, host_edge2, edge_agg1, edge_agg2, agg_core1, agg_core2);
	return connected_links;

}