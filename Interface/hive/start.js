var width = 1000,
    height = 450,
    innerRadius = 40,
    outerRadius = 400,
    majorAngle = 2 * Math.PI / 3,
    minorAngle = 1 * Math.PI / 12;

var pLoss = false;
var cong = false;
var showAll = true;                        

var nodes2 = [];
var links2 = [];

var pLossNodeType1;
var pLossNode1;

var pLossNodeType1;
var pLossNode1;
var showAnomaly = 0;

var congestedLink;
var pLossLink;

var angle = d3.scale.ordinal()
  .domain(["host", "edge", "agg", "core"])
  .range([0, majorAngle - minorAngle, majorAngle + minorAngle, 2 * majorAngle]);

var radius = d3.scale.linear()
    .range([innerRadius, outerRadius]);

var color = d3.scale.category20c();

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([0,0])
  .html(function(d) {return d.name;});

//Link Popup
var tip1 = d3.tip()
  .attr('class', 'd3-tip')
  .html(function(d) { return d.source.node.name + " → " + d.target.node.name;});

var zooms = d3.behavior.zoom();

var svg = d3.select("#chart").append("svg")
    .attr("width", width)
    .attr("height", height)
    .call(zooms.center([0,0]).scaleExtent([1, 8]).on("zoom", zoom))
    .append("g")
    .attr("transform", "translate(" + outerRadius  + "," + 300 * 1 + ")")
    .call(tip1)
    .call(tip);

function zoom() {
  var translateX;
  var translateY;
  translateX = d3.event.translate[0] + 400;
  translateY = d3.event.translate[1] + 300;
  svg.attr("transform", "translate(" + translateX + "," + translateY +") scale(" + d3.event.scale +")");
}

var traffic;
var links = [];
var nodes;
var nodesByType = {};
var slider;
var nodesByName = {};

slider = d3.slider();
d3.select('#slider5').call(slider.axis(true).min(-90).max(0).value(-1));

var val = callSlider();

d3.select("#packetLoss").on("change", function(d){packetLoss();});
d3.select("#congestion").on("change", function(d){congestion();});
d3.select("#all").on("change", function(d){all();});

var dropdown = d3.select("#files");
var selectedFile = "k6";
dropdown.on("change", function(d){
  selectedFile = dropdown[0][0].value;
  showFile(selectedFile);
});

// Load the data and display the plot!
parseJSON("k6.json");


function getLinks(){
  return links;
}

function getPLoss(){
  return pLoss;
}

function getTraffic(){
  return traffic;
}

function setTraffic(){
  traffic = makeConnections();
  pLossNodeType1 = Math.floor(Math.random() * 4);
  pLossNode1 = Math.floor(Math.random() * 5);
}

function getNodes(){
  return nodes;
}

function getNodesByType(){
  return nodesByType;
}

function callSlider(){
  var tempNodes = nodes;
  var tempNodesByType = nodesByType;

  slider.on("slide", function(evt, value){
    showAnomaly = Math.floor(Math.random() * 5);
    var tempLinks = links;
    setTraffic();
    tempTraffic = getTraffic();
    var trafficPloss;
    val = value;

    if(showAll === true){
     draw(tempNodes, tempLinks);
      drawConnections(tempTraffic);
    }
    else if(pLoss === true){
    trafficPloss = tempTraffic[pLossNodeType1][pLossNode1];
    updateData(trafficPloss);
    draw(nodes2, links2);
    drawConnections(tempTraffic);
    }
    else if(cong === true){
      trafficPloss = tempTraffic[0][0];
      updateData(trafficPloss);
      draw(nodes2,links2);
      drawConnections(tempTraffic);
    }
  });
  return val;
}



