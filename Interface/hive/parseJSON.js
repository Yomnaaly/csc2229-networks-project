function parseJSON(filename){
  nodes = {};
  links = [];
  nodesByName = {};
  nodesByType = {};

  d3.json(filename, function(n) {  
  nodes = n;
  var formatNumber = d3.format(",d"),
  defaultInfo;

// Construct an index by node name.
  nodes.forEach(function(d) {
    d.connectors = [];
    ip = d.name.split(".");
    d.ip = ip[2]+ ip[3];
    nodesByName[d.name] = d;
  });

  // Convert the import lists into links with sources and targets.
  nodes.forEach(function(node) {
    node.links.forEach(function(linkname) {
      var node2 = nodesByName[linkname];
      if (!node.source) {
        node.source = {node: node, degree: 0};
        node.connectors.push(node.source);
      }
      if (!node2.target){
        node2.target = {node: node2, degree: 0}
        node2.connectors.push(node2.target);
      }
      links.push({source: node.source, target: node2.target});
     });
  });

  // Nest nodes by type, for computing the rank.
   nodesByType = d3.nest()
      .key(function(d) { return d.type; })
      .sortKeys(d3.ascending)
      .entries(nodes);



  // Compute the rank for each type, with padding between packages.
  nodesByType.forEach(function(type) {
    var lastName = type.values[0].type, count = 0;
    type.values.forEach(function(d, i) {
      if (d.type != lastName) lastName = d.type, count += 2;
      d.index = count++;
    });
    type.count = count - 1;
  });

 
  
 draw(nodes,links);
  
 });

  return nodesByType;
}
